export const localUserServ = {
    get:()=>{
    let dataJson = localStorage.getItem("USER_INFOR");
    return JSON.parse(dataJson)
    },
    set:(userInfor)=>{
       let dataJson = JSON.stringify(userInfor);
       localStorage.setItem("USER_INFOR",dataJson)
    },
    remove:()=>{
    localStorage.removeItem("USER_INFOR");
    },
}