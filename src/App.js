import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import HomePage from './pages/HomePage/HomePage';
import LoginPage from './pages/LoginPage/LoginPage';
import Detailpage from './pages/DetailPage/Detailpage';
import Layout from './Layout/Layout';
import BookingPage from './pages/BookingPage/BookingPage';
import NotFoundPage from './pages/NotFoundPage/NotFoundPage';

function App() {
  return (
    <div >
      <BrowserRouter>
      <Routes>
        <Route path='/'  element={<HomePage/>} />;
        <Route path='/login'  element={<LoginPage/>} />
        <Route path='/detail/:id'  element={<Layout  Component={Detailpage}/>} />
        <Route path='/booking/:id'  element={<Layout Component={BookingPage}/>} />
        <Route path='/*'  element={<Layout  Component={NotFoundPage }/>} />

      </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
