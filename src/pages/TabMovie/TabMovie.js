import React, { useEffect, useState } from 'react'
import { movieServ } from '../../service/movieService'
import { Tabs } from 'antd';
import Itemmovie from '../HomePage/ListMovie/ItemMovie';
import ItemTabMovie from './ItemTabMovie';

export default function TabMovie() {
    const [heThongRap, setHeThongRap] = useState([]);
    useEffect(() => {
  movieServ.getMovieByTheater() 
  .then((res) => {
          console.log(res);
          setHeThongRap(res.data.content)
        })
        .catch((err) => {
         console.log(err);
        });
    }, []);
    let renderHeThongrap = ()=>{
      return heThongRap.map((rap)=>{
        return {
            key: rap.maHeThongRap,
            label: <img src={rap.logo} className="h-20"  alt="" />,
            children: <Tabs style={{height:600}} defaultActiveKey="1" 
            items={rap.lstCumRap.map((cumRap)=>{
              return {
                key:cumRap.tenCumRap,
                label:<div>{cumRap.tenCumRap}</div>,
                children:<div className='overflow-y-scroll ' style={{height:600}}>
                    {cumRap.danhSachPhim.map((item)=>{
                  return <ItemTabMovie movie={item} />
                })}
                </div>
              
              }
            })} tabPosition="left"
             onChange={onChange} /> ,
        }
      })
    }
const onChange = (key) => {
  console.log(key);
};

    
  return (
    <div className='container p-11 '>
      <table className='border-2 border-orange-200'>
      <Tabs className='container p-11  rounded-xl' style={{height:700}}  defaultActiveKey="1" 
        items={renderHeThongrap()} tabPosition="left"
         onChange={onChange} />
      </table>
    </div>
  )
}
