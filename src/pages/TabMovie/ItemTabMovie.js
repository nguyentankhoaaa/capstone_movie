import React from 'react';
import moment from "moment";

export default function ItemTabMovie({movie}) {
  return (
    <div className='p-5 flex' >
    <img src={movie.hinhAnh} className="w-36 h-36 object-cover" alt="" />
    <div>
        <h3 className='font-medium ml-3 text-gray-800 text-xl'>{movie.tenPhim}</h3>
    <div className='grid grid-cols-3 gap-1 m-3'>
        {movie.lstLichChieuTheoPhim.slice(0,9).map((item)=>{
            return <span className='text-white rounded p-2 bg-red-500'>
                {moment(item.ngayChieuGioChieu).format("DD-MM-YYYY~hh:mm ")}
            </span>
        })}
    </div>
    </div>
    </div>
  )
}
