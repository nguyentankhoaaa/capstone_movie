import { Button, Checkbox, Form, Input, message } from 'antd';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { USER_LOGIN } from '../../redux/constant/userConsstant';
import { localUserServ } from '../../service/localService';
import { userServ } from '../../service/userService';
import Lottie from "lottie-react";
import animate_Json  from "../../asset/animate_Login.json";
import { setLoginAction, setLoginActionServ } from '../../redux/action/userAtion';

const LoginPage = () => {
let navigate = useNavigate();
let dispatch = useDispatch()
    // const onFinish = (values) => {
    //     console.log('Success:', values);
    //     userServ.postLogin(values)
    //     .then((res) => {
    //             console.log(res);
    //             message.success("Đăg nhập thành công!");
    //             localUserServ.set(res.data.content);
    //             navigate("/");
    //             dispatch(setLoginAction(res.data.content))
    //           })
    //           .catch((err) => {
    //            console.log(err);
    //            message.error("Đăng nhập thất bại!")
               
    //           });
    //   };
      const onFinishThunk = (value)=>{
        let onSuccess = ()=>{
          message.success("Đăg nhập thành công!");
         navigate("/");
        }
        dispatch(setLoginActionServ(value,onSuccess))
    console.log(value);
      }
      const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
      };   
    return (
        <div className="h-screen w-screen bg-gray-800  p-16 ">
                <h2 className='font-medium text-8xl text-center text-red-500 animate-pulse'><i class="fa fa-film"></i> CyberFlix</h2>
                
            <div className=' mx-auto p-11 mt-12   rounded-2xl bg-gray-400 flex'>
              <div className="w-1/2 h-full">
              <Lottie animationData={animate_Json} style={{height:350,width:600}} loop={true} />
              </div>
              <div className="w-1/2 h-full">
                <Form layout='vertical' className='mt-16'
                  name="basic"
                  labelCol={{
                    span: 8,
                  }}
                  wrapperCol={{
                    span: 24,
                  }}
                  style={{
                    width:"90%",
                  }}
                  initialValues={{
                    remember: true,
                  }}
                  onFinish={onFinishThunk}
                  onFinishFailed={onFinishFailed}
                  autoComplete="off"
                >
                  <Form.Item
                    label="Username"
                    name="taiKhoan"
                    rules={[
                      {
                        required: true,
                        message: 'Please input your username!',
                      },
                    ]}
                  >
                    <Input />
                  </Form.Item>
                  <Form.Item
                    label="Password"
                    name="matKhau"
                    rules={[
                      {
                        required: true,
                        message: 'Please input your password!',
                      },
                    ]}
                  >
                    <Input.Password />
                  </Form.Item>
                  <Form.Item  className='text-right'
                    wrapperCol={{
                     
                      span: 24,
                    }}
                  >
                    <Button className='bg-slate-100' htmlType="submit">
                      Submit
                    </Button>
                  </Form.Item>
                </Form>
              </div>
            </div>
        </div>
      );
        
    
}
export default LoginPage;