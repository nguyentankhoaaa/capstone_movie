import React from 'react'
import Header from '../../Components/Header/Header'
import TabMovie from '../TabMovie/TabMovie'
import ListMovie from './ListMovie/ListMovie'

export default function HomePage() {
  return (
    <div className='bg-gray-100'>
      <Header/>
        <ListMovie/>
        <hr className='bg-white container shadow-4xl ' />
        <TabMovie/>
    </div>
  )
}
