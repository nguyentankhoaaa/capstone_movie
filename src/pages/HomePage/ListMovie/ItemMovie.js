import { Card } from 'antd';
import {NavLink} from "react-router-dom";
const { Meta } = Card;
const Itemmovie = ({movie}) => (
  <Card
    hoverable
    cover={<img className='h-72 object-cover object-top' alt="example" src={movie.hinhAnh}/>}

  >
    <Meta title={movie.tenPhim} description={<NavLink to={`/detail/${movie.maPhim}`}><button className='font-medium text-blue-400'>Xem ngay</button></NavLink>} />
  </Card>
);
export default Itemmovie;