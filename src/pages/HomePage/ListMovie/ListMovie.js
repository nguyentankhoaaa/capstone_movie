import React, { useEffect, useState } from 'react'
import { movieServ } from '../../../service/movieService'
import ItemMovie from './ItemMovie';

export default function ListMovie() {
   const [movies, setMovies] = useState([]);
    useEffect(() => {
        movieServ.getmovieList()
        .then((res) => {
                console.log(res);
                setMovies(res.data.content)
              })
              .catch((err) => {
               console.log(err);
              });
    }, [])
    
  return (
    <div className='container p-11 grid grid-cols-4 gap-10'>
   {movies.map((item)=>{
    return <ItemMovie movie={item} key={item.maPhim}/>
   })}
    </div>
  )
}
