import { Progress } from 'antd';
import React, { useEffect, useState } from 'react'
import { NavLink, useParams } from 'react-router-dom'
import { movieServ } from '../../service/movieService';

export default function Detailpage() {
    let {id} = useParams();
    const [movie, setMovie] = useState([])
    useEffect (() => {
let fetchDetail = async()=>{
  try {
    let result = await movieServ.getDetailMovie(id);
    setMovie(result.data.content)
    console.log(result);
    
  } catch (error) {
    console.log(error);
  }
}
fetchDetail()
}, [])

  return (
    <div className=' container '>
<div className="flex space-x-10 mb-2 my-5">
<img src={movie.hinhAnh} alt="" style={{height:400}} className='w-1/3 object-contain shadow-xl rounded-xl' />
      <div className='space-y-5'>
        <h2 className='font-medium text-2xl'>{movie.tenPhim}</h2>
        <h2 className='text-blue-700 font-medium'> Nội dung Phim: <span className='text-black font-thin'>{movie.moTa}</span></h2>
        <h2 className='text-blue-700 font-medium'> Lượt đánh giá: <span> <Progress  className='w-1/2' percent={movie.danhGia*10} /></span></h2>
      </div>
</div>
      <NavLink  className="rounded px-5 py-1 ml-4 bg-red-500 text-white font-medium "  to={`/booking/${id}`}>
        Mua Vé
      </NavLink>
      <button className='bg-blue-500 text-white px-5 py-1 rounded ml-4'>Xem Trailler</button>
    </div>
  )
}
