import { localUserServ } from "../../service/localService";
import { userServ } from "../../service/userService";
import { USER_LOGIN } from "../constant/userConsstant";

export const setLoginAction = (value)=>{
   return {
    type:USER_LOGIN,
    payload:value,   
};
};
export const setLoginActionServ = (value,onCompleted)=>{
     return (dispatch) => { 
        userServ.postLogin(value).then((res) => {
        
                console.log(res);
                dispatch({
                    type:USER_LOGIN,
                    payload:value,   
                });
                localUserServ.set(res.data.content);
                onCompleted()
              })
              .catch((err) => {
               console.log(err);
              });
      }
}