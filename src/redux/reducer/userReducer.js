import { localUserServ } from "../../service/localService"
import { USER_LOGIN } from "../constant/userConsstant"

const initialState = {
    userInfor:localUserServ.get()
}

let  userReducer = (state = initialState, action) => {
  switch (action.type) {
   case USER_LOGIN:{
    return {...state,userInfor:action.payload}
   }


  default:
    return state
  }
}
export default userReducer