import { DownOutlined } from '@ant-design/icons';
import { Dropdown, Space } from 'antd';
const USer_Dropdown = ({user,logOutBtn}) => {
    const items = [
      {
        label: <p className='text-blue-500 p-2'><i class="fa fa-user"></i> Thông Tin Tài khoản</p> ,
        key: '0',
      },
      {
        label: <p className='text-blue-500 p-2'><i class="fa fa-history"></i>  Lịch sử xem phim</p> ,
        key: '0',
      },
      {
        label: logOutBtn,
        key: '1',
      },
    
    ];
return (
    <Dropdown
      menu={{
        items,
      }}
      trigger={['click']}
    >
      <a onClick={(e) => e.preventDefault()}>
        <Space className='text-green-600 font-medium mr-5'>
          {user.hoTen}
          <DownOutlined />
        </Space>
      </a>
    </Dropdown>
  );
}
    

export default USer_Dropdown;