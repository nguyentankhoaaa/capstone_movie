import React from 'react'
import { useSelector } from 'react-redux'
import { NavLink } from 'react-router-dom';
import { localUserServ } from '../../service/localService';
import USer_Dropdown from './User_Dropdown';

export default function UserMenu() {
  let userInfor = useSelector((state)=>{
    return state.userReducer.userInfor;
  })
  
  let renderContent = () => { 
  let buttonCSS = "px-5 mr-2 py-2 font-medium border-2 rounded-xl border-red-600 text-red-600 "
    if(userInfor){
     return <>
     <USer_Dropdown user={userInfor} logOutBtn={<button className={buttonCSS}  onClick={handleLogOut}>  Đăng Xuất</button>}/>
   
     </>
    }
    else{
        return <>
          <NavLink to="/login">
          <button className={buttonCSS}> Đăng nhập</button>
          </NavLink>
          <button className={buttonCSS}>Đăng ký</button>
        </>
    }

}

let handleLogOut = () => { 
    localUserServ.remove();
    window.location.reload();
 }
  console.log(userInfor);
  return (
    <div>

{renderContent()}
    </div>
  )
}
