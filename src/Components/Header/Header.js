import React from 'react'
import { Desktop, Mobile, Tablet } from '../../Layout/Responsive'
import Header_Desktop from './Header_Desktop'
import Header_Mobile from './Header_Mobile'
import Header_Tablet from './Header_Tablet'

export default function Header() {
  return (
    <div>
     <Desktop>
      <Header_Desktop/>
     </Desktop>
     <Tablet>
      <Header_Tablet/>
     </Tablet>
     <Mobile>
      <Header_Mobile/>
     </Mobile>

    </div>
  )
}
