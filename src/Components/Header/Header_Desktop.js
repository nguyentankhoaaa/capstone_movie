import React from 'react'
import { NavLink } from 'react-router-dom'
import UserMenu from './UserMenu'

export default function Header_Desktop() {
  return (
    <div className='h-20 shadow w-full'>
        <div className="container mx-auto h-full flex justify-between items-center p-11">
         <NavLink to="/" className="animate-bounce">
         <span className='font-medium text-4xl text-red-500 '><i class="fa fa-film"></i> CyberFlix</span>
         </NavLink>
          <UserMenu/>
        </div>
    </div>
  )
}
